package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    
   BankController bankcon = new BankController();
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 100;
  
   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;

   public InvestmentFrame(){  
      // Use instance variables for components 
      resultLabel = new JLabel("balance: " + bankcon.getBalanceAccount());

      // Use helper methods 
      createTextField();
      createButton();
      createPanel();
      
      setVisible(true);
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
   }

   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + bankcon.DEFAULT_RATE());
   }
   
   private void createButton()
   {
      button = new JButton("Add Interest");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
            double rate = Double.parseDouble(rateField.getText());
            double interest = bankcon.getBalanceAccount() * rate / 100;
            bankcon.getDeposit(interest);
            resultLabel.setText("balance: " + bankcon.getBalanceAccount());
         }            
      }

      ActionListener listener = new AddInterestListener();
      button.addActionListener(listener);
   }

   
   public double rateField(){
		return rateField();
	}
   
   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   } 
}
