package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class BankController {
	 private static final double DEFAULT_RATE = 5;
	 private static final double INITIAL_BALANCE = 1000; 
	 private BankAccount account;
	 double balance;
	 
	 public static void main(String[] args){
		 new InvestmentFrame();
	   }
	 public BankController(){
		 account = new BankAccount(INITIAL_BALANCE);
	 }
	 
	 public double getBalanceAccount(){
			return account.getBalance();
		}
	 public void getDeposit(double interest){
			account.deposit(interest);
		}
	 
	 public double DEFAULT_RATE(){
			return DEFAULT_RATE;
		}
	 public double INITIAL_BALANCE(){
			return INITIAL_BALANCE;
		}

}